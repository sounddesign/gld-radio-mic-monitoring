# README #

### What is this repository for? ###

* If using a prefade aux output from the A&H GLD mixing console, this script will generate a Qlab 4 file that you can run to select which radio mic you are monitoring from the monitor station. 
* 1.2

### How do I get set up? ###

* Open the included template in Qlab 4
* Run the script. Select the mix number (as shown in http://www.allen-heath.com/media/GLD-MIDI-and-TCPIP-Protocol-V1.4\_2.pdf � ensure it is the correct mix number), and the first and last inputs which you wish to monitor.
* Once complete, move the contents of the "Cue Cart" group into the cue cart that has been created. You can use this on a computer or an iPad to choose which mic you are monitoring.