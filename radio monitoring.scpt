on moveCue(currentCue, group)
	tell application "QLab 4"
		set currentCueID to uniqueID of currentCue
		move cue id currentCueID of parent of currentCue to end of group
	end tell
end moveCue

on routeChannel(inputNumber, mixNumber, inputStartNumber, inputEndNumber)
	tell application id "com.figure53.QLab.4" to tell front workspace
		-- create main group
		make type "Group"
		set masterGroup to last item of (selected as list)
		set q number of masterGroup to inputNumber
		set q name of masterGroup to "Unmute Channel " & inputNumber
		set mode of masterGroup to fire_first_go_to_next_cue
		set continue mode of masterGroup to do_not_continue
		-- create mute
		make type "Network"
		set cueMute to last item of (selected as list)
		set q_num of cueMute to "mute"
		set q_command of cueMute to 1
		set post wait of cueMute to 0.03
		set continue mode of cueMute to auto_continue
		-- create MIDI 1 (Select Input Channel)
		make type "Midi"
		set cueMidi1 to last item of (selected as list)
		set q name of cueMidi1 to "Select Input Channel"
		set command of cueMidi1 to control_change
		set channel of cueMidi1 to 1
		set byte one of cueMidi1 to 99
		set byte two of cueMidi1 to (31 + inputNumber)
		set post wait of cueMidi1 to 0.01
		set continue mode of cueMidi1 to auto_continue
		-- create MIDI 2 (Select Output Mix)
		make type "Midi"
		set cueMidi2 to last item of (selected as list)
		set q name of cueMidi2 to "Select Output Mix"
		set command of cueMidi2 to control_change
		set channel of cueMidi2 to 1
		set byte one of cueMidi2 to 98
		set byte two of cueMidi2 to (31 + mixNumber)
		set post wait of cueMidi2 to 0.01
		set continue mode of cueMidi2 to auto_continue
		-- create MIDI 3 (Select Level)
		make type "Midi"
		set cueMidi3 to last item of (selected as list)
		set q name of cueMidi3 to "Select Level"
		set command of cueMidi3 to control_change
		set channel of cueMidi3 to 1
		set byte one of cueMidi3 to 6
		set byte two of cueMidi3 to 100
		set post wait of cueMidi3 to 0.01
		set continue mode of cueMidi3 to auto_continue
		-- create arm mute
		make type "Network"
		set cueArmMute to last item of (selected as list)
		set q_num of cueArmMute to "mute_" & inputNumber
		set q_command of cueArmMute to 20
		set q_params of cueArmMute to 1
		set pre wait of cueArmMute to 0.1
		
	end tell
	
	moveCue(cueMute, masterGroup)
	moveCue(cueMidi1, masterGroup)
	moveCue(cueMidi2, masterGroup)
	moveCue(cueMidi3, masterGroup)
	moveCue(cueArmMute, masterGroup)
end routeChannel

on muteChannel(inputNumber, mixNumber, groupMute, inputStartNumber)
	tell application id "com.figure53.QLab.4" to tell front workspace
		-- group
		make type "Group"
		set cueMuteInput to last item of (selected as list)
		set q number of cueMuteInput to "mute_" & inputNumber
		set q name of cueMuteInput to "Mute Channel " & inputNumber
		set mode of cueMuteInput to fire_first_go_to_next_cue
		set continue mode of cueMuteInput to do_not_continue
		-- Midi 1
		make type "Midi"
		set cueMidi1 to last item of (selected as list)
		set q name of cueMidi1 to "Select Input Channel"
		set command of cueMidi1 to control_change
		set channel of cueMidi1 to 1
		set byte one of cueMidi1 to 99
		set byte two of cueMidi1 to (31 + inputNumber)
		set post wait of cueMidi1 to 0.01
		set continue mode of cueMidi1 to auto_continue
		-- create MIDI 2 (Select Output Mix)
		make type "Midi"
		set cueMidi2 to last item of (selected as list)
		set q name of cueMidi2 to "Select Output Mix"
		set command of cueMidi2 to control_change
		set channel of cueMidi2 to 1
		set byte one of cueMidi2 to 98
		set byte two of cueMidi2 to (31 + mixNumber)
		set post wait of cueMidi2 to 0.01
		set continue mode of cueMidi2 to auto_continue
		-- create MIDI 3 (Select Level)
		make type "Midi"
		set cueMidi3 to last item of (selected as list)
		set q name of cueMidi3 to "Select Level"
		set command of cueMidi3 to control_change
		set channel of cueMidi3 to 1
		set byte one of cueMidi3 to 6
		set byte two of cueMidi3 to 0
		set post wait of cueMidi3 to 0.01
		set continue mode of cueMidi3 to auto_continue
		-- create disarm mute
		make type "Network"
		set cueDisarmMute to last item of (selected as list)
		set q_num of cueDisarmMute to "mute_" & inputNumber
		set q_command of cueDisarmMute to 20
		set q_params of cueDisarmMute to 0
		
	end tell
	moveCue(cueMuteInput, groupMute)
	moveCue(cueMidi1, cueMuteInput)
	moveCue(cueMidi2, cueMuteInput)
	moveCue(cueMidi3, cueMuteInput)
	moveCue(cueDisarmMute, cueMuteInput)
end muteChannel

on triggerChannel(inputNumber, cueCart)
	tell application "QLab 4" to tell front workspace
		make type "Network"
		set cueTrigger to last item of (selected as list)
		set q name of cueTrigger to "Unmute Channel " & inputNumber
		set q_num of cueTrigger to inputNumber
		set q_command of cueTrigger to 1
	end tell
	moveCue(cueTrigger, cueCart)
end triggerChannel

tell application "QLab 4" to tell front workspace
	
	-- Global set Output Mix Channel
	display dialog "Select Mix Number:" default answer "M Grp, St Grp, M FX, M Aux, St FX, St Aux, Main" with title "Mix Number" with icon 1 buttons {"Cancel", "OK"} default button "OK" cancel button "Cancel"
	set mixNumber to text returned of result
	
	-- Set input channel range
	display dialog "Select First Input Channel:" default answer "Input" with title "First Input Channel" with icon 1 buttons {"Cancel", "OK"} default button "OK" cancel button "Cancel"
	set inputStartNumber to text returned of result
	
	display dialog "Select Last Input Channel:" default answer "Input" with title "Last Input Channel" with icon 1 buttons {"Cancel", "OK"} default button "OK" cancel button "Cancel"
	set inputEndNumber to text returned of result
	
	-- make mute group
	make type "Group"
	set groupMute to last item of (selected as list)
	set q number of groupMute to "mute"
	set q name of groupMute to "Mute all channels"
	set mode of groupMute to fire_all
	
	-- make cue cart
	make type "cue cart"
	
	make type "group"
	set cueCart to last item in (selected as list)
	set q name of cueCart to "cue cart"
	
	-- create disarm all mutes
	make type "network"
	set cueDisarmAll to last item of (selected as list)
	set q name of cueDisarmAll to "Reset"
	set q number of cueDisarmAll to "reset"
	set q color of cueDisarmAll to "Red"
	set q_num of cueDisarmAll to "mute_*"
	set q_command of cueDisarmAll to 20
	set q_params of cueDisarmAll to 0
	
end tell

repeat with inputNumber from inputStartNumber to inputEndNumber
	routeChannel(inputNumber, mixNumber, inputStartNumber, inputEndNumber)
	muteChannel(inputNumber, mixNumber, groupMute, inputStartNumber)
	triggerChannel(inputNumber, cueCart)
end repeat

tell application "QLab 4" to tell front workspace
	make type "Network"
	set cueTrigger to last item of (selected as list)
	set q name of cueTrigger to "MUTE ALL"
	set q color of cueTrigger to "Red"
	set q_num of cueTrigger to "mute"
	set q_command of cueTrigger to 1
end tell
moveCue(cueTrigger, cueCart)

tell application "QLab 4" to tell front workspace
	start cueDisarmAll
end tell